package com.mitocode.evaluacion;

import com.mitocode.evaluacion.dao.IUsuarioDao;
import com.mitocode.evaluacion.model.Usuario;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import static junit.framework.TestCase.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MediappBackendEvaluacionApplicationTests {

    @Autowired
    private IUsuarioDao dao;
    @Autowired
    private BCryptPasswordEncoder bcrypt;

    @Test
    public void crearUsuario() {
      Usuario usuario = new Usuario();
        usuario.setIdUsuario(3);
        usuario.setUsername("dba");
        usuario.setPassword(bcrypt.encode("dba"));
        usuario.setEnabled(true);

        Usuario retorno = dao.save(usuario);
        assertTrue(retorno.getPassword().equalsIgnoreCase((usuario.getPassword())));
    }

}
