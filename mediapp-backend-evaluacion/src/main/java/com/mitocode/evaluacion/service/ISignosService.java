package com.mitocode.evaluacion.service;

import com.mitocode.evaluacion.dto.FiltroSignoDTO;
import com.mitocode.evaluacion.model.SignoPaciente;

import java.util.List;

public interface ISignosService extends ICRUD<SignoPaciente> {
    List<SignoPaciente> buscar(FiltroSignoDTO filtro);

    List<SignoPaciente> buscarFecha(FiltroSignoDTO filtro);
}
