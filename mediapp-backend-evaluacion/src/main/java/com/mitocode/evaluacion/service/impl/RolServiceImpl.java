package com.mitocode.evaluacion.service.impl;

import com.mitocode.evaluacion.dao.IRolDao;
import com.mitocode.evaluacion.model.Rol;
import com.mitocode.evaluacion.service.IRolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RolServiceImpl implements IRolService {

    @Autowired
    private IRolDao dao;


    @Override
    public Rol registrar(Rol rol) {
        return dao.save(rol);
    }

    @Override
    public Rol modificar(Rol rol) {
        return dao.save(rol);
    }

    @Override
    public Rol listarId(Integer id) {
        Optional<Rol> opt = dao.findById(id);
        return opt.isPresent() ? opt.get() : new Rol();
    }

    @Override
    public List<Rol> listar() {
        return dao.findAll();
    }

    @Override
    public void eliminar(Integer id) {
        dao.deleteById(id);
    }
}
