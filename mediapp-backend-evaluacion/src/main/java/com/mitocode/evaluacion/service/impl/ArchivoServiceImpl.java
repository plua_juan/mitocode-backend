package com.mitocode.evaluacion.service.impl;


import com.mitocode.evaluacion.dao.IArchivoDao;
import com.mitocode.evaluacion.model.Archivo;
import com.mitocode.evaluacion.service.IArchivoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ArchivoServiceImpl implements IArchivoService {

    @Autowired
    private IArchivoDao dao;

    @Override
    public int guardar(Archivo archivo) {
        Archivo ar = dao.save(archivo);
        return ar.getIdArchivo() > 0 ? 1 : 0;
    }

    @Override
    public byte[] leerArchivo(Integer idArchivo) {
        Optional<Archivo> opt = dao.findById(idArchivo);
        return opt.isPresent() ? opt.get().getValue() : new byte[0];
    }

}
