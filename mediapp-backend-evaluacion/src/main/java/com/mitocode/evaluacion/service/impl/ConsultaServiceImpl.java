package com.mitocode.evaluacion.service.impl;

import com.mitocode.evaluacion.dao.IConsultaDao;
import com.mitocode.evaluacion.dao.IConsultaExamenDao;
import com.mitocode.evaluacion.dto.ConsultaListaExamenDTO;
import com.mitocode.evaluacion.dto.ConsultaResumenDTO;
import com.mitocode.evaluacion.dto.FiltroConsultaDTO;
import com.mitocode.evaluacion.model.Consulta;
import com.mitocode.evaluacion.service.IConsultaService;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ConsultaServiceImpl implements IConsultaService {

    @Autowired
    private IConsultaDao dao;

    @Autowired
    private IConsultaExamenDao ceDao;

    @Transactional
    @Override
    public Consulta registrarTransaccional(ConsultaListaExamenDTO consultaDTO) {
        consultaDTO.getConsulta().getDetalleConsulta().forEach(det -> det.setConsulta(consultaDTO.getConsulta()));
        dao.save(consultaDTO.getConsulta());

        consultaDTO.getListExamen().forEach(ex -> ceDao.registrar(consultaDTO.getConsulta().getIdConsulta(), ex.getIdExamen()));
        return consultaDTO.getConsulta();
    }

    @Override
    public List<Consulta> buscar(FiltroConsultaDTO filtro) {
        return dao.buscar(filtro.getDni(), filtro.getNombreCompleto());
    }

    @Override
    public List<Consulta> buscarFecha(FiltroConsultaDTO filtro) {
        LocalDateTime fechaSgte = filtro.getFechaConsulta().plusDays(1);
        return dao.buscarFecha(filtro.getFechaConsulta(), fechaSgte);
    }

    @Override
    public List<ConsultaResumenDTO> listarResumen() {
        List<ConsultaResumenDTO> consultas = new ArrayList<>();
        // List<Object[]>
        // cantidad fecha
        // [4 , 11/05/2019]
        // [1 , 18/05/2019]
        dao.listarResumen().forEach(x -> {
            ConsultaResumenDTO cr = new ConsultaResumenDTO();
            cr.setCantidad(Integer.parseInt(String.valueOf(x[0])));
            cr.setFecha(String.valueOf(x[1]));
            consultas.add(cr);
        });
        return consultas;
    }

    @Override
    public byte[] generarReporte() {
        byte[] data = null;

        //HashMap<String, String> params = new HashMap<String, String>();
        //params.put("txt_empresa", "MitoCode Network");

        try {
            File file = new ClassPathResource("/reports/consultas.jasper").getFile();
            JasperPrint print = JasperFillManager.fillReport(file.getPath(), null, new JRBeanCollectionDataSource(this.listarResumen()));
            data = JasperExportManager.exportReportToPdf(print);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }


    @Override
    public Consulta registrar(Consulta obj) {
        obj.getDetalleConsulta().forEach(det -> {
            det.setConsulta(obj);
        });
		
		/*for(DetalleConsulta det : obj.getDetalleConsulta()) {
			det.setConsulta(obj);
		}*/
        return dao.save(obj);
    }

    @Override
    public Consulta modificar(Consulta obj) {
        return dao.save(obj);
    }

    @Override
    public Consulta listarId(Integer id) {
        Optional<Consulta> opt = dao.findById(id);
        return opt.isPresent() ? opt.get() : new Consulta();
    }

    @Override
    public List<Consulta> listar() {
        return dao.findAll();
    }

    @Override
    public void eliminar(Integer id) {
        dao.deleteById(id);
    }

}
