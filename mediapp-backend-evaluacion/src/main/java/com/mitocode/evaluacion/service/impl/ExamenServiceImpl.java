package com.mitocode.evaluacion.service.impl;

import com.mitocode.evaluacion.dao.IExamenDao;
import com.mitocode.evaluacion.model.Examen;
import com.mitocode.evaluacion.service.IExamenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ExamenServiceImpl implements IExamenService {

    @Autowired
    private IExamenDao dao;


    @Override
    public Examen registrar(Examen examen) {
        return dao.save(examen);
    }

    @Override
    public Examen modificar(Examen examen) {
        return dao.save(examen);
    }

    @Override
    public Examen listarId(Integer id) {
        Optional<Examen> opt = dao.findById(id);
        return opt.isPresent() ? opt.get() : new Examen();
    }

    @Override
    public List<Examen> listar() {
        return dao.findAll();
    }

    @Override
    public void eliminar(Integer id) {
        dao.deleteById(id);
    }
}
