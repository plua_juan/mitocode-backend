package com.mitocode.evaluacion.service;

import com.mitocode.evaluacion.model.Menu;

import java.util.List;

public interface IMenuService extends ICRUD<Menu>  {
  /*  void registrar(Menu menu);

    void modificar(Menu menu);

    void eliminar(int idMenu);

    Menu listarId(int idMenu);

    List<Menu> listar();
*/
    List<Menu> listarMenuPorUsuario(String nombre);
}
