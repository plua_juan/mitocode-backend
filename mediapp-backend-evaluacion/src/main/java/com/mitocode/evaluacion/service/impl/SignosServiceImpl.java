package com.mitocode.evaluacion.service.impl;

import com.mitocode.evaluacion.dao.ISignosDao;
import com.mitocode.evaluacion.dto.FiltroSignoDTO;
import com.mitocode.evaluacion.model.SignoPaciente;
import com.mitocode.evaluacion.service.ISignosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class SignosServiceImpl implements ISignosService {

    @Autowired
    private ISignosDao dao;


    @Override
    public SignoPaciente registrar(SignoPaciente signo) {
        return dao.save(signo);
    }

    @Override
    public SignoPaciente modificar(SignoPaciente signo) {
        return dao.save(signo);
    }

    @Override
    public SignoPaciente listarId(Integer id) {
        Optional<SignoPaciente> opt = dao.findById(id);
        return opt.isPresent() ? opt.get() : new SignoPaciente();
    }

    @Override
    public List<SignoPaciente> listar() {
        return dao.findAll();
    }

    @Override
    public void eliminar(Integer id) {
        dao.deleteById(id);
    }

    @Override
    public List<SignoPaciente> buscar(FiltroSignoDTO filtro) {
        return dao.buscar(filtro.getDni(),filtro.getNombreCompleto());
    }

    @Override
    public List<SignoPaciente> buscarFecha(FiltroSignoDTO filtro) {
        LocalDateTime fechaSgte = filtro.getFechaConsulta().plusDays(1);
        return dao.buscarFecha(filtro.getFechaConsulta(), fechaSgte);
    }
}
