package com.mitocode.evaluacion.service.impl;

import com.mitocode.evaluacion.dao.IEspecialidadDao;
import com.mitocode.evaluacion.model.Especialidad;
import com.mitocode.evaluacion.service.IEspecialidadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EspecialidadServiceImpl implements IEspecialidadService {

    @Autowired
    private IEspecialidadDao dao;


    @Override
    public Especialidad registrar(Especialidad especialidad) {
        return dao.save(especialidad);
    }

    @Override
    public Especialidad modificar(Especialidad especialidad) {
        return dao.save(especialidad);
    }

    @Override
    public Especialidad listarId(Integer id) {
        Optional<Especialidad> opt = dao.findById(id);
        return opt.isPresent() ? opt.get() : new Especialidad();
    }

    @Override
    public List<Especialidad> listar() {
        return dao.findAll();
    }

    @Override
    public void eliminar(Integer id) {
        dao.deleteById(id);
    }
}
