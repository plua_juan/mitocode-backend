package com.mitocode.evaluacion.service;

import com.mitocode.evaluacion.model.Especialidad;

public interface IEspecialidadService extends ICRUD<Especialidad> {

}
