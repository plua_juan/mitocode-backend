package com.mitocode.evaluacion.service;

import com.mitocode.evaluacion.model.Archivo;

public interface IArchivoService {

    int guardar(Archivo archivo);
    byte[] leerArchivo(Integer idArchivo);
}
