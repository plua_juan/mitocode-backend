package com.mitocode.evaluacion.service.impl;

import com.mitocode.evaluacion.model.Paciente;
import com.mitocode.evaluacion.dao.IPacienteDao;
import com.mitocode.evaluacion.service.IPacienteService;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PacienteServiceImpl implements IPacienteService {

    @Autowired
    private IPacienteDao dao;


    @Override
    public Paciente registrar(Paciente paciente) {
        return dao.save(paciente);
    }

    @Override
    public Paciente modificar(Paciente paciente) {
        return dao.save(paciente);
    }

    @Override
    public Paciente listarId(Integer id) {
        Optional<Paciente> opt = dao.findById(id);
        return opt.isPresent() ? opt.get() : new Paciente();
    }

    @Override
    public List<Paciente> listar() {
        return dao.findAll();
    }

    @Override
    public void eliminar(Integer id) {
        dao.deleteById(id);
    }

    @Override
    public Page<Paciente> listarPageable(Pageable pageable) {
        return dao.findAll(pageable);
    }
}
