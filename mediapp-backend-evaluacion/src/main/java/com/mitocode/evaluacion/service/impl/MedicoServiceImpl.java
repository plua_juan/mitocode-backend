package com.mitocode.evaluacion.service.impl;

import com.mitocode.evaluacion.dao.IMedicoDao;
import com.mitocode.evaluacion.model.Medico;
import com.mitocode.evaluacion.service.IMedicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MedicoServiceImpl implements IMedicoService {

    @Autowired
    private IMedicoDao dao;


    @Override
    public Medico registrar(Medico medico) {
        return dao.save(medico);
    }

    @Override
    public Medico modificar(Medico medico) {
        return dao.save(medico);
    }

    @Override
    public Medico listarId(Integer id) {
        Optional<Medico> opt = dao.findById(id);
        return opt.isPresent() ? opt.get() : new Medico();
    }

    @Override
    public List<Medico> listar() {
        return dao.findAll();
    }

    @Override
    public void eliminar(Integer id) {
        dao.deleteById(id);
    }
}
