package com.mitocode.evaluacion.service;

import com.mitocode.evaluacion.dto.ConsultaListaExamenDTO;
import com.mitocode.evaluacion.dto.ConsultaResumenDTO;
import com.mitocode.evaluacion.dto.FiltroConsultaDTO;
import com.mitocode.evaluacion.model.Consulta;

import java.util.List;

public interface IConsultaService extends ICRUD<Consulta> {
    Consulta registrarTransaccional(ConsultaListaExamenDTO consultaDTO);
    List<Consulta> buscar(FiltroConsultaDTO filtro);

    List<Consulta> buscarFecha(FiltroConsultaDTO filtro);

    List<ConsultaResumenDTO> listarResumen();

    byte[] generarReporte();
}
