package com.mitocode.evaluacion.service;

import com.mitocode.evaluacion.model.Paciente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IPacienteService extends ICRUD<Paciente> {
    Page<Paciente> listarPageable(Pageable pageable);
}
