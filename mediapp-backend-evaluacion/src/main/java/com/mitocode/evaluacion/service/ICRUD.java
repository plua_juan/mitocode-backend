package com.mitocode.evaluacion.service;

import java.util.List;

public interface ICRUD<T> {
    T registrar(T t);

    T modificar(T t);

    T listarId(Integer id);

    List<T> listar();

    void eliminar(Integer id);
}
