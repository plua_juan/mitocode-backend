package com.mitocode.evaluacion.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class SignoPaciente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idSigno;

    @ManyToOne
    @JoinColumn(name = "id_paciente", nullable = false, foreignKey = @ForeignKey(name = "fk_Signos_paciente"))
    private Paciente paciente;
    @JsonSerialize(using = ToStringSerializer.class) // ISODate
    @Column(name = "fecha", nullable = false)
    private LocalDateTime fecha;
    @Column(name = "temperatura", nullable = false, length = 9)
    private String temperatura;
    @Column(name = "pulso", nullable = false, length = 9)
    private String pulso;
    @Column(name = "ritmoRespiratorio", nullable = false, length = 9)
    private String ritmo;

    public Integer getIdSigno() {
        return idSigno;
    }

    public void setIdSigno(Integer idSigno) {
        this.idSigno = idSigno;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }


    public String getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(String temperatura) {
        this.temperatura = temperatura;
    }

    public String getPulso() {
        return pulso;
    }

    public void setPulso(String pulso) {
        this.pulso = pulso;
    }

    public String getRitmo() {
        return ritmo;
    }

    public void setRitmo(String ritmoRespiratorio) {
        this.ritmo = ritmoRespiratorio;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((idSigno == null) ? 0 : idSigno.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SignoPaciente other = (SignoPaciente) obj;
        if (idSigno == null) {
			return other.idSigno == null;
        } else return idSigno.equals(other.idSigno);
	}

}
