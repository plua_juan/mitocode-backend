package com.mitocode.evaluacion.controller;

import com.mitocode.evaluacion.exception.ModelNotFoundException;
import com.mitocode.evaluacion.model.Examen;
import com.mitocode.evaluacion.service.IExamenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;


@RestController
@RequestMapping("/examenes")
public class ExamenController {

    @Autowired
    private IExamenService service;

    @GetMapping
    public ResponseEntity<List<Examen>> listar() {
        List<Examen> lista = service.listar();
        return new ResponseEntity<List<Examen>>(lista, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Examen> listarId(@PathVariable("id") Integer id) {
        Examen examen = new Examen();
        examen = service.listarId(id);
        if (examen == null) {
            throw new ModelNotFoundException("ID: " + id);
        }
        return new ResponseEntity<Examen>(examen, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Object> registrar(@RequestBody Examen examen) {
        Examen pac = new Examen();
        pac = service.registrar(examen);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(pac.getIdExamen()).toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> modificar(@RequestBody Examen examen) {
        service.modificar(examen);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id) {
        Examen examen = service.listarId(id);
        if (examen == null) {
            throw new ModelNotFoundException("ID NO ENCONTRADO: " + id);
        } else {
            service.eliminar(id);
        }
        return new ResponseEntity<Object>(examen, HttpStatus.OK);
    }


}
