package com.mitocode.evaluacion.controller;

import com.mitocode.evaluacion.dto.ConsultaListaExamenDTO;
import com.mitocode.evaluacion.dto.ConsultaResumenDTO;
import com.mitocode.evaluacion.dto.FiltroConsultaDTO;
import com.mitocode.evaluacion.exception.ModelNotFoundException;
import com.mitocode.evaluacion.model.Archivo;
import com.mitocode.evaluacion.model.Consulta;
import com.mitocode.evaluacion.service.IArchivoService;
import com.mitocode.evaluacion.service.IConsultaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/consultas")
public class ConsultaController {

    @Autowired
    private IConsultaService service;

    @Autowired
    private IArchivoService serviceArchivo;

    @GetMapping
    public ResponseEntity<List<Consulta>> listar() {
        List<Consulta> lista = service.listar();
        return new ResponseEntity<List<Consulta>>(lista, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Consulta> leerPorId(@PathVariable("id") Integer id) {
        Consulta obj = service.listarId(id);
        if (obj == null) {
            throw new ModelNotFoundException("ID NO ENCONTRADO: " + id);
        }
        return new ResponseEntity<Consulta>(obj, HttpStatus.OK);
    }


    @PostMapping
    public ResponseEntity<Object> registrar(@Valid @RequestBody ConsultaListaExamenDTO obj) {
        Consulta paciente = service.registrarTransaccional(obj);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(paciente.getIdConsulta()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping
    public ResponseEntity<Object> modificar(@Valid @RequestBody Consulta pac) {
        service.modificar(pac);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id) {
        Consulta obj = service.listarId(id);
        if (obj == null) {
            throw new ModelNotFoundException("ID NO ENCONTRADO: " + id);
        } else {
            service.eliminar(id);
        }
        return new ResponseEntity<Object>(obj, HttpStatus.OK);
    }

    @PostMapping("/buscar")
    public ResponseEntity<List<Consulta>> buscar(@RequestBody FiltroConsultaDTO filtro) {
        List<Consulta> consultas = new ArrayList<>();

        if (filtro != null) {
            if (filtro.getFechaConsulta() != null) {
                consultas = service.buscarFecha(filtro);
            } else {
                consultas = service.buscar(filtro);
            }
        }
        return new ResponseEntity<List<Consulta>>(consultas, HttpStatus.OK);
    }

    @GetMapping(value = "/listarResumen")
    public ResponseEntity<List<ConsultaResumenDTO>> listarResumen() {
        List<ConsultaResumenDTO> consultas = new ArrayList<>();
        consultas = service.listarResumen();
        return new ResponseEntity<List<ConsultaResumenDTO>>(consultas, HttpStatus.OK);
    }

    @GetMapping(value = "/generarReporte", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<byte[]> generarReporte() {
        byte[] data = null;
        data = service.generarReporte();
        return new ResponseEntity<byte[]>(data, HttpStatus.OK);
    }

    @PostMapping(value = "/guardarArchivo", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<Integer> guardarArchivo(@RequestParam("file") MultipartFile file) throws IOException {
        int rpta = 0;
        Archivo ar = new Archivo();
        ar.setFiletype(file.getContentType());
        ar.setFilename(file.getName());
        ar.setValue(file.getBytes());
        rpta = serviceArchivo.guardar(ar);

        return new ResponseEntity<Integer>(rpta, HttpStatus.OK);
    }

    @GetMapping(value = "/leerArchivo/{idArchivo}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<byte[]> leerArchivo(@PathVariable("idArchivo") Integer idArchivo) throws IOException {

        byte[] arr = serviceArchivo.leerArchivo(idArchivo);

        return new ResponseEntity<byte[]>(arr, HttpStatus.OK);
    }
}
