package com.mitocode.evaluacion.controller;

import com.mitocode.evaluacion.dto.FiltroSignoDTO;
import com.mitocode.evaluacion.exception.ModelNotFoundException;
import com.mitocode.evaluacion.model.SignoPaciente;
import com.mitocode.evaluacion.service.ISignosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/signosVitales")
public class SignoController {

    @Autowired
    private ISignosService service;

    @GetMapping
    public ResponseEntity<List<SignoPaciente>> listar() {
        List<SignoPaciente> lista = service.listar();
        return new ResponseEntity<List<SignoPaciente>>(lista, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SignoPaciente> listarId(@PathVariable("id") Integer id) {
        SignoPaciente signo = new SignoPaciente();
        signo = service.listarId(id);
        if (signo == null) {
            throw new ModelNotFoundException("ID: " + id);
        }
        return new ResponseEntity<SignoPaciente>(signo, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Object> registrar(@RequestBody SignoPaciente signo) {
        SignoPaciente pac = new SignoPaciente();
        pac = service.registrar(signo);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(pac.getIdSigno()).toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> modificar(@RequestBody SignoPaciente signo) {
        service.modificar(signo);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id) {
        SignoPaciente signo = service.listarId(id);
        if (signo == null) {
            throw new ModelNotFoundException("ID NO ENCONTRADO: " + id);
        } else {
            service.eliminar(id);
        }
        return new ResponseEntity<Object>(signo, HttpStatus.OK);
    }

    @PostMapping("/buscar")
    public ResponseEntity<List<SignoPaciente>> buscar(@RequestBody FiltroSignoDTO filtro) {
        List<SignoPaciente> signoPacientes = new ArrayList<>();
        if (filtro != null) {
            if (filtro.getDni() != null || filtro.getFechaConsulta() != null || filtro.getNombreCompleto() != null) {
                if (filtro.getFechaConsulta() != null) {
                    signoPacientes = service.buscarFecha(filtro);
                } else {
                    signoPacientes = service.buscar(filtro);
                }
            } else {
                signoPacientes = service.listar();
            }

        }
        return new ResponseEntity<List<SignoPaciente>>(signoPacientes, HttpStatus.OK);
    }

}
