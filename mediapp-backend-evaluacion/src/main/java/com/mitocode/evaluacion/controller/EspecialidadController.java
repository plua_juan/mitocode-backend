package com.mitocode.evaluacion.controller;

import com.mitocode.evaluacion.exception.ModelNotFoundException;
import com.mitocode.evaluacion.model.Especialidad;
import com.mitocode.evaluacion.service.IEspecialidadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;


@RestController
@RequestMapping("/especialidades")
public class EspecialidadController {

    @Autowired
    private IEspecialidadService service;

    @GetMapping
    public ResponseEntity<List<Especialidad>> listar() {
        List<Especialidad> lista = service.listar();
        return new ResponseEntity<List<Especialidad>>(lista, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Especialidad> listarId(@PathVariable("id") Integer id) {
        Especialidad especialidad = new Especialidad();
        especialidad = service.listarId(id);
        if (especialidad == null) {
            throw new ModelNotFoundException("ID: " + id);
        }
        return new ResponseEntity<Especialidad>(especialidad, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Object> registrar(@RequestBody Especialidad especialidad) {
        Especialidad pac = new Especialidad();
        pac = service.registrar(especialidad);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(pac.getIdEspecialidad()).toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> modificar(@RequestBody Especialidad especialidad) {
        service.modificar(especialidad);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id) {
        Especialidad especialidad = service.listarId(id);
        if (especialidad == null) {
            throw new ModelNotFoundException("ID NO ENCONTRADO: " + id);
        } else {
            service.eliminar(id);
        }
        return new ResponseEntity<Object>(especialidad, HttpStatus.OK);
    }


}
