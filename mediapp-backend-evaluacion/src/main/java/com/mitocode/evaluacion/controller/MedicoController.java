package com.mitocode.evaluacion.controller;

import com.mitocode.evaluacion.exception.ModelNotFoundException;
import com.mitocode.evaluacion.model.Medico;
import com.mitocode.evaluacion.service.IMedicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;


@RestController
@RequestMapping("/medicos")
public class MedicoController {

    @Autowired
    private IMedicoService service;
    @PreAuthorize("@restAuthService.hasAccess('listar')")
    @GetMapping
    public ResponseEntity<List<Medico>> listar() {
        List<Medico> lista = service.listar();
        return new ResponseEntity<List<Medico>>(lista, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Medico> listarId(@PathVariable("id") Integer id) {
        Medico medico = new Medico();
        medico = service.listarId(id);
        if (medico == null) {
            throw new ModelNotFoundException("ID: " + id);
        }
        return new ResponseEntity<Medico>(medico, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Object> registrar(@RequestBody Medico medico) {
        Medico pac = new Medico();
        pac = service.registrar(medico);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(pac.getIdMedico()).toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> modificar(@RequestBody Medico medico) {
        service.modificar(medico);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id) {
        Medico medico = service.listarId(id);
        if (medico == null) {
            throw new ModelNotFoundException("ID NO ENCONTRADO: " + id);
        } else {
            service.eliminar(id);
        }
        return new ResponseEntity<Object>(medico, HttpStatus.OK);
    }


}
