package com.mitocode.evaluacion.controller;

import com.mitocode.evaluacion.exception.ModelNotFoundException;
import com.mitocode.evaluacion.model.Paciente;
import com.mitocode.evaluacion.service.IPacienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@RestController
@RequestMapping("/pacientes")
public class PacienteController {

    @Autowired
    private IPacienteService service;

    @GetMapping
    public ResponseEntity<List<Paciente>> listar() {
        List<Paciente> lista = service.listar();
        return new ResponseEntity<List<Paciente>>(lista, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Paciente> listarId(@PathVariable("id") Integer id) {
        Paciente paciente = new Paciente();
        paciente = service.listarId(id);
        if (paciente == null) {
            throw new ModelNotFoundException("ID: " + id);
        }
        return new ResponseEntity<Paciente>(paciente, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Object> registrar(@Valid @RequestBody Paciente paciente) {
        Paciente pac = new Paciente();
        pac = service.registrar(paciente);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(pac.getIdPaciente()).toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> modificar( @Valid @RequestBody Paciente paciente) {
        service.modificar(paciente);
        return new ResponseEntity<Object>(HttpStatus.OK);

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id) {
        Paciente paciente = service.listarId(id);
        if (paciente == null) {
            throw new ModelNotFoundException("ID NO ENCONTRADO: " + id);
        } else {
            service.eliminar(id);
        }
        return new ResponseEntity<Object>(paciente, HttpStatus.OK);
    }

    @GetMapping("/pageable")
    public ResponseEntity<Page<Paciente>> listarPageable(Pageable pageable) {
        Page<Paciente> pacientes = service.listarPageable(pageable);
        return new ResponseEntity<Page<Paciente>>(pacientes, HttpStatus.OK);
    }

}
