package com.mitocode.evaluacion.controller;

import com.mitocode.evaluacion.exception.ModelNotFoundException;
import com.mitocode.evaluacion.model.Rol;
import com.mitocode.evaluacion.service.IRolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/roles")
public class RolController {

    @Autowired
    private IRolService service;

    @GetMapping
    public ResponseEntity<List<Rol>> listar() {
        List<Rol> lista = service.listar();
        return new ResponseEntity<List<Rol>>(lista, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Rol> listarId(@PathVariable("id") Integer id) {
        Rol rol = new Rol();
        rol = service.listarId(id);
        if (rol == null) {
            throw new ModelNotFoundException("ID: " + id);
        }
        return new ResponseEntity<Rol>(rol, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Object> registrar(@RequestBody Rol obj) {
        Rol rol = new Rol();
        rol = service.registrar(obj);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(rol.getIdRol()).toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> modificar(@RequestBody Rol rol) {
        service.modificar(rol);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id) {
        Rol rol = service.listarId(id);
        if (rol == null) {
            throw new ModelNotFoundException("ID NO ENCONTRADO: " + id);
        } else {
            service.eliminar(id);
        }
        return new ResponseEntity<Object>(rol, HttpStatus.OK);
    }


}
