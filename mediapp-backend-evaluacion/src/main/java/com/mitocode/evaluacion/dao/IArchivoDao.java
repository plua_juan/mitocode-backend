package com.mitocode.evaluacion.dao;

import com.mitocode.evaluacion.model.Archivo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IArchivoDao extends JpaRepository<Archivo, Integer> {

}
