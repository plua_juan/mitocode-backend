package com.mitocode.evaluacion.dao;

import com.mitocode.evaluacion.model.Rol;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IRolDao extends JpaRepository<Rol,Integer> {
}
