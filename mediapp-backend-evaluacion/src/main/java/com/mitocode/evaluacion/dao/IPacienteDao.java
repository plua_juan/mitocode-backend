package com.mitocode.evaluacion.dao;

import com.mitocode.evaluacion.model.Paciente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPacienteDao extends JpaRepository<Paciente,Integer> {
}
