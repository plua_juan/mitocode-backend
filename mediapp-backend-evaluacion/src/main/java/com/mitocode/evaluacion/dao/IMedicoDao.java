package com.mitocode.evaluacion.dao;

import com.mitocode.evaluacion.model.Medico;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IMedicoDao extends JpaRepository<Medico,Integer> {
}
