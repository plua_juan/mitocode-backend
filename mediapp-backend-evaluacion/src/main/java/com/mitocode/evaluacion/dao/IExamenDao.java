package com.mitocode.evaluacion.dao;

import com.mitocode.evaluacion.model.Examen;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IExamenDao extends JpaRepository<Examen,Integer> {
}
