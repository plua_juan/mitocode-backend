package com.mitocode.evaluacion.dao;

import com.mitocode.evaluacion.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUsuarioDao extends JpaRepository<Usuario,Integer> {
    Usuario findOneByUsername(String username);
}
