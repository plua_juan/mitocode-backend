package com.mitocode.evaluacion.dao;

import com.mitocode.evaluacion.model.SignoPaciente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface ISignosDao extends JpaRepository<SignoPaciente, Integer> {
    @Query("from SignoPaciente c where c.paciente.dni =:dni or LOWER(c.paciente.nombres) like %:nombreCompleto% or LOWER(c.paciente.apellidos) like %:nombreCompleto%")
    List<SignoPaciente> buscar(@Param("dni") String dni, @Param("nombreCompleto") String nombreCompleto);

    @Query("from SignoPaciente c where c.fecha between :fechaConsulta and :fechaSgte")
    List<SignoPaciente> buscarFecha(@Param("fechaConsulta") LocalDateTime fechaConsulta, @Param("fechaSgte") LocalDateTime fechaSgte);


}
