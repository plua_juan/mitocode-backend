package com.mitocode.evaluacion.dao;

import com.mitocode.evaluacion.model.Especialidad;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IEspecialidadDao extends JpaRepository<Especialidad,Integer> {
}
