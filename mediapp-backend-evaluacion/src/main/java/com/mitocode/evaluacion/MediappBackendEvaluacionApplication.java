package com.mitocode.evaluacion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MediappBackendEvaluacionApplication {

    public static void main(String[] args) {
        SpringApplication.run(MediappBackendEvaluacionApplication.class, args);
    }

}
